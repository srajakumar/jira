# -*- coding: utf-8 -*-
"""
Created on Thu Jun 28 11:51:24 2018

@author: rajaks
"""
import sys
import argparse
import re
import numpy as np
import matplotlib.pyplot as plt
from itertools import chain
#import matplotlib.ticker as 

class ExtData:
    
    def __init__(self,prewiredfn,postwiredfn):
        self.pre_wired_name = prewiredfn
        self.post_wired_name = postwiredfn
        self.pre_data_lst=[]
        self.post_data_lst=[]
        
    def preWired(self):
        with open(self.pre_wired_name,'r') as pre:
            for line in pre:
                # Capture ITCP, EUDP, ETCP output
                if re.search(r'SUM',line) and re.search(r'sender',line) or re.search(r'SUM',line) and  '%' in line:
                    #print line
                    temp = line.split("  ")
                    #BLock to convert the list and get the data in float format
                    for val in temp:
                        if 'bits/sec' in val:
                            val=val.strip().split(" ")
                            temp=float(val[0])
                            self.pre_data_lst.append(temp)
                # Capture IWGET and EWGET output
                elif re.search(r'/dev/null',line) and '/s)' in line:
                    line=line.replace('B/s','bits/sec')
                    temp=float(line.split('(',1)[1].split(")")[0].split(" ")[0])
                    self.pre_data_lst.append(temp)
                    
                    #print tval
        #self.pre_data_lst = list(chain.from_iterable(self.pre_data_lst))
        
        return self.pre_data_lst
               
    
    def postWired(self):
        with open(self.post_wired_name,'r') as pre:
            for line in pre:
                # Capture ITCP, EUDP, ETCP output
                if re.search(r'SUM',line) and re.search(r'sender',line) or re.search(r'SUM',line) and  '%' in line:
                    temp = line.split("  ")
                    #BLock to convert the list and get the data in float format
                    for val in temp:
                        if 'bits/sec' in val:
                            val=val.strip().split(" ")
                            temp=float(val[0])
                            self.post_data_lst.append(temp)
                # Capture IWGET and EWGET output
                elif re.search(r'/dev/null',line) and '/s)' in line:
                    line= line.replace('B/s','bits/sec')
                    temp=float(line.split('(',1)[1].split(")")[0].split(" ")[0])
                    self.post_data_lst.append(temp)
        
        return self.post_data_lst
    
    def dataGraph(self):
        
        lenlst = len(self.pre_data_lst)
        
        #Create common layout of subplots returns figure and axes object
        fig,ax = plt.subplots()
        
        #Index - To position the bars with given alignment
        index=np.arange(lenlst)
        bar_width=0.40
        opacity=0.6
        error_config = {'ecolor': '0.3'}
        
        #Block 1 which shows the pre-data bar graph       
        rects1 = ax.bar(index, self.pre_data_lst, bar_width, alpha=opacity, color='b',  error_kw=error_config, label='Pre-wired')
        #Block to have the value labelled over the graph
        for rect in rects1:
            h = rect.get_height()
            ax.text(rect.get_x(), 1*h, '%0.2f'%float(h),
                ha='center', va='bottom')
        
        #Block 2 which shows the post-data bar graph
        rects2 = ax.bar(index + bar_width, self.post_data_lst, bar_width,alpha=opacity, color='r', error_kw=error_config,label='Post-wired')
        #Block to have the value labelled over the graph
        for rect in rects2:
            h = rect.get_height()
            ax.text(rect.get_x()+rect.get_width(), h, '%0.2f'%float(h),
                ha='center', va='bottom')
        
        ax.set_xlabel('Test case groups')
        ax.set_ylabel('Bandwidth in (MB)(GB)/s')
        ax.set_title('Bandwidth comparison for pre/post wired test case')
        ax.set_xticks(index + bar_width / 2)
        ax.set_xticklabels(('ITCP', 'EUDP', 'ETCP', 'Iwget', 'Ewget'))
        ax.legend()
        
        
        
        fig.tight_layout()
        plt.show()
        
        
            


def main(args):
    
    
    parser = argparse.ArgumentParser(description="Extract Download and Upload data from notepad file to form graph")
    parser.add_argument('pre_wired_filename',help="Name of the pre wired output file")
    parser.add_argument('post_wired_filename',help="Name of the post wired output file")
    
    
    
    #Parse the arguments
    args=parser.parse_args()
    
    
    dataobj = ExtData(args.pre_wired_filename,args.post_wired_filename)
    #Call the preWired method in Class ExtData
    dataobj.preWired()
    
    
    #Call the preWired method in Class ExtData
    dataobj.postWired()
   
    
    #Call the method which creates the bar graph
    dataobj.dataGraph()
    

if __name__ == '__main__':
    main(sys.argv[1:])
    